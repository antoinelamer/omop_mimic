-- Demography

-- Number of patient per sex

select c.concept_name, count(*) 
from omop.person p
inner join omop.concept c 
on p.gender_concept_id = c.concept_id
group by c.concept_name 
order by c.concept_name;

-- Age at admission

select age(vo.visit_start_date, p.birth_datetime ) as age_at_admission
from omop.person p 
inner join omop.visit_occurrence vo
on p.person_id = vo.person_id;

-- Nombre d'enregistrements par type d'observation

select c.concept_id, c.concept_name, count(*) 
from omop.observation p
inner join omop.concept c 
on p.observation_concept_id = c.concept_id
group by c.concept_id, c.concept_name 
order by c.concept_id, c.concept_name;

-- Nombre d'enregistrements par type de diagnostic

select c.concept_id, c.concept_name, count(*) 
from omop.condition_occurrence p
inner join omop.concept c 
on p.condition_concept_id = c.concept_id
group by c.concept_id, c.concept_name 
order by count(*) desc;

-- Nombre d'enregistrements par type de procédure

select c.concept_id, c.concept_name, count(*) 
from omop.procedure_occurrence p
inner join omop.concept c 
on p.procedure_concept_id = c.concept_id
group by c.concept_id, c.concept_name 
order by count(*) desc;

-- Nombre d'enregistrements par type de mesure

select c.concept_id, c.concept_name, count(*) 
from omop.measurement p
inner join omop.concept c 
on p.measurement_concept_id = c.concept_id
group by c.concept_id, c.concept_name 
order by count(*) desc;